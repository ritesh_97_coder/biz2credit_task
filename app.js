const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const { MONGODB } = require('./config');
const userRoutes = require('./routes/user');
const noteRoutes = require('./routes/note');
const auth = require('./middleware/auth');

app.use(bodyParser.json());
app.use('/api/vi/notes', auth, noteRoutes);
// app.use('/api/v1/protected', auth, (req, res) => {
//     res.end(`Hi ${req.user.name}, you are authenticated user!`);
// });

app.use('/api/v1/users', userRoutes);

app.use((req, res, next) => {
    const err = new Error('not found');
    err.status = 404;
    next(err);
});
app.use((err, req, res, next) => {
    const status = err.status || 500;
    res.status(status).json({ error: { message: err.message } });
});
app.get('/', (req, res, next) => res.end('Welcome to My Task'));

mongoose.connect(MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to mongodb');
        return app.listen(3300);
    })
    .then(() => console.log('Server running at port 3300'))
    .catch(err => console.log(err.message));